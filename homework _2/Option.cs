﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework__2
{
    public abstract class Option
    {
        public Underlying Underlying { get; set; }
        public DateTime ExpiryDate { get; set; }
        public Random Ran { get; set; }

        public double ObservedVolatility { get; set; }
        /*public abstract double GetPrice();
        public abstract double GetDelta();*/

    }
    public sealed class EuOption : Option
    {
        public double Strike { get; set; }
        public double[] GetPrice(int t, int n, double s, double k, double r, double T, double sigma, double[,] ran, bool Isanti, bool Deltac)
        {
            if (Isanti && (!Deltac))//USE ANTI TO CALCULATE
            {
                double[] PriceandSE = new double[4];
                double[] path = Simulator.EUSimulateAnti(t, n, s, k, r, T, sigma, ran);
                double[] path1 = new double[2 * n];//PATH TO CALL
                double[] path2 = new double[2 * n];//PATH TO CALL
                double Callprice = 0;
                double Putprice = 0;
                for (int i = 0; i < 2 * n; i++)
                {
                    path1[i] = Math.Max(path[i] - k, 0);
                }
                for (int i = 0; i < 2 * n; i++)
                {
                    path2[i] = Math.Max(k - path[i], 0);
                }
                double total1 = 0;
                double total2 = 0;
                for (int i = 0; i < 2 * n; i++)
                {
                    total1 = total1 + path1[i];
                    total2 = total2 + path2[i];
                }
                double[] conbine1 = new double[n];// USE TO CALCULATE ANTI SE FOR CALL
                double[] conbine2 = new double[n];// USE TO CALCULATE ANTI SE FOR PUT
                for (int i = 0; i < n; i++)
                {
                    conbine1[i] = (path1[i] + path1[i + n]) / 2;
                    conbine2[i] = (path2[i] + path2[i + n]) / 2;
                }
                double mean1 = total1 / 2 / n;
                double mean2 = total2 / 2 / n;
                double SEhelper1 = 0;
                double SEhelper2 = 0;
                for (int i = 0; i < n; i++)
                {
                    SEhelper1 = SEhelper1 + (conbine1[i] - mean1) * (conbine1[i] - mean1);
                    SEhelper2 = SEhelper2 + (conbine2[i] - mean2) * (conbine2[i] - mean2);
                }
                double sdCall = Math.Sqrt(SEhelper1 / (n - 1) / n) * Math.Exp(-r * T);
                double sdPut = Math.Sqrt(SEhelper2 / (n - 1) / n) * Math.Exp(-r * T);
                Callprice = mean1 * Math.Exp(-r * T);
                Putprice = mean2 * Math.Exp(-r * T);

                PriceandSE[0] = Callprice;
                PriceandSE[1] = sdCall;
                PriceandSE[2] = Putprice;
                PriceandSE[3] = sdPut;
                return PriceandSE;
            }
            else if ((!Isanti) && (!Deltac))
            {// NOT USE ANTI TO CALCULATE
                double[] PriceandSE = new double[4];
                double[] path = Simulator.EUSimulateNormal(t, n, s, k, r, T, sigma, ran);
                double[] path1 = new double[n];
                double[] path2 = new double[n];
                double Callprice = 0;
                double Putprice = 0;
                for (int i = 0; i < n; i++)
                {
                    path1[i] = Math.Max(path[i] - k, 0);
                }
                for (int i = 0; i < n; i++)
                {
                    path2[i] = Math.Max(k - path[i], 0);
                }
                double total1 = 0;
                double total2 = 0;
                for (int i = 0; i < n; i++)
                {
                    total1 = total1 + path1[i];
                    total2 = total2 + path2[i];
                }
                double mean1 = total1 / n;
                double mean2 = total2 / n;
                double SEhelper1 = 0;
                double SEhelper2 = 0;
                for (int i = 0; i < n; i++)
                {
                    SEhelper1 = SEhelper1 + (path1[i] - mean1) * (path1[i] - mean1);
                    SEhelper2 = SEhelper2 + (path2[i] - mean2) * (path2[i] - mean2);
                }
                double sdCall = Math.Sqrt(SEhelper1 / (n - 1) / n) * Math.Exp(-r * T);
                double sdPut = Math.Sqrt(SEhelper2 / (n - 1) / n) * Math.Exp(-r * T);
                Callprice = mean1 * Math.Exp(-r * T);
                Putprice = mean2 * Math.Exp(-r * T);

                PriceandSE[0] = Callprice;
                PriceandSE[1] = sdCall;
                PriceandSE[2] = Putprice;
                PriceandSE[3] = sdPut;
                return PriceandSE;
            }
            else if (Isanti && Deltac)
            {// this is method which use delta hedge and anti 

                double[] PriceandSE = new double[4];
                double[,] path = Simulator.SimulateAnti(t, n, s, k, r, T, sigma, ran);


                double sum_CTc = 0;
                double sum_CTc2 = 0;
                double sum_CTp = 0;
                double sum_CTp2 = 0;

                //double[] delta = new double[t];
                double delta1 = 0;
                double delta2 = 0;
                double d1 = 0;
                double d2 = 0;
                double CT1 = 0;
                double CT2 = 0;
                double Beta1 = -1;
                for (int i = 0; i < n; i++)
                {
                    double cvc1 = 0;
                    double cvc2 = 0;
                    double cvp1 = 0;
                    double cvp2 = 0;

                    for (int j = 0; j < t - 1; j++)
                    {
                        d1 = (Math.Log(path[i, j] / k) + (r + 0.5 * sigma * sigma) * (T - j * T / (t - 1))) / (sigma * Math.Sqrt(T - j * T / (t - 1)));
                        d2= (Math.Log(path[i+n, j] / k) + (r + 0.5 * sigma * sigma) * (T - j * T / (t - 1))) / (sigma * Math.Sqrt(T - j * T / (t - 1)));
                        delta1 = Cdf.Phi(d1);
                        delta2 = Cdf.Phi(d2);
                        cvc1 = delta1 * (path[i, j + 1] - path[i, j] * Math.Exp(r * T / (t - 1))) + cvc1;
                        cvc2= delta2 * (path[i+n, j + 1] - path[i+n, j] * Math.Exp(r * T / (t - 1))) + cvc2;
                        cvp1 = (delta1 - 1) * (path[i, j + 1] - path[i, j] * Math.Exp(r * T / (t - 1))) + cvp1;
                        cvp2 = (delta2 - 1) * (path[i+n, j + 1] - path[i+n, j] * Math.Exp(r * T / (t - 1))) + cvp2;
                    }

                    CT1 = (Math.Max(path[i, t - 1] - k, 0) + Beta1 * cvc1+ Math.Max(path[i+n, t - 1] - k, 0) + Beta1 * cvc2) * 0.5;
                    CT2 = (Math.Max(k-path[i, t - 1] , 0) + Beta1 * cvp1+ Math.Max(k-path[i+n, t - 1], 0) + Beta1 * cvp2) * 0.5;
                    sum_CTc = sum_CTc + CT1;
                    sum_CTc2 = sum_CTc2 + CT1 * CT1;
                    sum_CTp = sum_CTp + CT2;
                    sum_CTp2 = sum_CTp2 + CT2 * CT2;
                }
               
               
                
                    double Callprice = sum_CTc / n * Math.Exp(-r * T );
                    double sdCall = Math.Sqrt((sum_CTc2 - sum_CTc * sum_CTc / n) * Math.Exp(-2 * r * T) / n / (n - 1));
                    double Putprice = sum_CTp / n * Math.Exp(-r * T );
                    double sdPut = Math.Sqrt((sum_CTp2 - sum_CTp * sum_CTp / n) * Math.Exp(-2 * r * T) / n / (n - 1));


                PriceandSE[0] = Callprice;
                    PriceandSE[1] = sdCall;
                    PriceandSE[2] = Putprice;
                    PriceandSE[3] = sdPut;
                    return PriceandSE;
                }

            
            else
            {//thisis a method which only use delta hedge
                double[] PriceandSE = new double[4];
                double[,] path = Simulator.SimulateNormal(t, n, s, k, r, T, sigma, ran);


                double sum_CTc = 0;
                double sum_CTc2 = 0;
                double sum_CTp = 0;
                double sum_CTp2 = 0;

                //double[] delta = new double[t];
                double delta = 0;
                double d = 0;
                double CT1 = 0;
                double CT2 = 0;
                double Beta1 = -1;
                for (int i = 0; i < n; i++)
                {
                    double cv1 = 0;
                    double cv2 = 0;

                    for (int j = 0; j < t - 1; j++)
                    {
                        d = (Math.Log(path[i, j] / k) + (r + 0.5 * sigma * sigma) * (T - j * T / (t - 1))) / (sigma * Math.Sqrt(T - j * T / (t - 1)));

                        delta = Cdf.Phi(d);
                        cv1 = delta * (path[i, j + 1] - path[i, j] * Math.Exp(r * T / (t - 1)))+cv1;
                        cv2 = (delta - 1) * (path[i, j + 1] - path[i, j] * Math.Exp(r * T / (t - 1)))+cv2;
                       
                    }
                    CT1 = (Math.Max(path[i, t - 1] - k, 0) + Beta1 * cv1);
                        CT2 = (Math.Max(k-path[i, t - 1], 0) + Beta1 * cv2);
                        sum_CTc = sum_CTc + CT1;
                        sum_CTc2 = sum_CTc2 + CT1 * CT1;
                        sum_CTp = sum_CTp + CT2;
                        sum_CTp2 = sum_CTp2 + CT2 * CT2;
                }
                    double Callprice = sum_CTc / n * Math.Exp(-r * T );
                    double sdCall = Math.Sqrt((sum_CTc2 - sum_CTc * sum_CTc / n) * Math.Exp(-2 * r * T )/ n / (n - 1));
                    double Putprice = sum_CTp / n * Math.Exp(-r * T);
                    double sdPut = Math.Sqrt((sum_CTp2 - sum_CTp * sum_CTp / n) * Math.Exp(-2 * r * T) / n / (n - 1));


                PriceandSE[0] = Callprice;
                    PriceandSE[1] = sdCall;
                    PriceandSE[2] = Putprice;
                    PriceandSE[3] = sdPut;
                    return PriceandSE;
                }
            }
        
        public double[] GetGreek(int t, int n, double s, double k, double r, double T, double sigma, double[,] ran, bool Isanti,bool Deltac)

        {// USE TO CALCULATE GREEK FOR EUR CALL AND PUT
            double[] Price1 = GetPrice(t, n, s, k, r, T, sigma, ran, Isanti,Deltac);
            double[] Delta1 = GetPrice(t, n, 1.001 * s, k, r, T, sigma, ran, Isanti,Deltac);
            double[] Gmma1 = GetPrice(t, n, 0.999 * s, k, r, T, sigma, ran, Isanti,Deltac);
            double[] Theta1 = GetPrice(t, n, s, k, r, 1.001 * T, sigma, ran, Isanti,Deltac);
            double[] Rho1 = GetPrice(t, n, s, k, 1.0001 * r, T, sigma, ran, Isanti,Deltac);
            double[] Vega1 = GetPrice(t, n, s, k, r, T, 1.0001 * sigma, ran, Isanti,Deltac);
            double callD = (Delta1[0] - Price1[0]) / (0.001 * s);
            double callG = (Gmma1[0] + Delta1[0] - 2 * Price1[0]) / (0.001 * 0.001 * s * s);
            double callT = -(Theta1[0] - Price1[0]) / (0.001 * T);
            double callR = (Rho1[0] - Price1[0]) / (0.0001 * r);
            double callV = (Vega1[0] - Price1[0]) / (0.0001 * sigma);
            double putD = (Delta1[2] - Price1[2]) / (0.001 * s);
            double putG = (Gmma1[2] + Delta1[2] - 2 * Price1[2]) / (0.001 * 0.001 * s * s);
            double putT = -(Theta1[2] - Price1[2]) / (0.001 * T);
            double putR = (Rho1[2] - Price1[2]) / (0.0001 * r);
            double putV = (Vega1[2] - Price1[2]) / (0.0001 * sigma);

            double[] price = new double[10];

            price[0] = callD;
            price[1] = callG;
            price[2] = callT;
            price[3] = callR;
            price[4] = callV;

            price[5] = putD;
            price[6] = putG;
            price[7] = putT;
            price[8] = putR;
            price[9] = putV;

            return price;//return the result
        }

    }
}

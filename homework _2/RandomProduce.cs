﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework__2
{
    class RandomProduce
    {
        public static double ran1 = 0, ran2 = 0;

        public static double[,] GetrandP(int n, int t)
        {
            double[,] rand = new double[n, t];
            double w, c;
            Random rnd = new Random();
            for (int i = 0; i < n; i = i + 2)
            {
                for (int j = 0; j < t; j++)
                {
                    do
                    {
                        ran1 = 2 * rnd.NextDouble() - 1;//get the random variables from -1 to 1
                        ran2 = 2 * rnd.NextDouble() - 1;//get the random variables from -1 to 1
                        w = ran1 * ran1 + ran2 * ran2;
                    }
                    while (w > 1);
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    rand[i, j] = c * ran1;
                    rand[i + 1, j] = c * ran2;
                }
            }
            return rand;

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework__2
{
    class Simulator
    {
        public static double[] EUSimulateNormal(int t, int n, double s, double k, double r, double T, double sigma, double[,] ran)
        {//SIMULATE THE STOCK PRICE FOR EUR CALL OR PUT AND NOT USE ANTI 
            double[] EUsimularOP = new double [n];

            for (int i = 0; i < n; i++)
            {
                EUsimularOP[i] = s;
            }
            for (int i = 0; i < n; i++)
            {
                for (int j = 1; j < t; j++)
                {
                    EUsimularOP[i] = EUsimularOP[i] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (T / (t-1)) + sigma * Math.Sqrt(T /( t-1)) * ran[i, j]);                    
                }

            }
            return EUsimularOP;// RETURN A 1*N MARTIX
        }
        public static double[] EUSimulateAnti(int t, int n, double s, double k, double r, double T, double sigma, double[,] ran)
        {//SIMULATE THE STOCK PRICE FOR EUR CALLOR PUT AND USE ANTI

            double[] EUsimularOP = new double[2*n];

            for (int i = 0; i <2* n; i++)
            {
                EUsimularOP[i] = s;
            }
            for (int i = 0; i < n; i++)
            {
                for (int j = 1; j < t; j++)
                {
                    EUsimularOP[i] = EUsimularOP[i] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (T / (t-1)) + sigma * Math.Sqrt(T /( t-1)) * ran[i, j]);
                    EUsimularOP[n+i] = EUsimularOP[i+n] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (T / (t-1)) + sigma * Math.Sqrt(T / (t-1)) * ran[i, j]*(-1));
                }

            }
            return EUsimularOP;


            /* double[] EUsimularOP = new double[n];
            double[] EusimularAnti = new double[2 * n];
            EUsimularOP1 = EUSimulateNormal(t, n, s, k, r, T, sigma, ran);
            for (int i = 0; i < n; i++)
            {
                EusimularAnti[i] = EUsimularOP[i];
                EusimularAnti[i + n] = EUsimularOP[i] * (-1);
            }
            return EusimularAnti;*/
        }

        public static double[,] SimulateNormal(int t, int n, double s, double k, double r, double T, double sigma, double[,] ran)
        {
            //SIMULATE THE STOCK PRICE FOR ALL KIND OF CALL OR PUT AND NOT USE ANTI
            double[,] simularOP = new double[n, t];
            /*double[,] simularT = new double[n, t];
            double[,] simularR = new double[n, t];
            double[,] simularV = new double[n, t];*/

            for (int i = 0; i < n; i++)
            {
                simularOP[i, 0] = s;
            }
            for (int i = 0; i < n; i++)
            {
                for (int j = 1; j < t; j++)
                {
                    simularOP[i, j] = simularOP[i, j - 1] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (T / (t-1)) + sigma * Math.Sqrt(T / (t-1)) * ran[i, j]);
                   // simulate the path of stock
                   /* simularOP[i+n, j] = simularOP[i+n, j - 1] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (1.001*T / t) + sigma * Math.Sqrt(1.001*T / t) * ran[i, j]);
                      simularOP[i+2*n, j] = simularOP[i+2*n, j - 1] * Math.Exp((1.0001*r - Math.Pow(sigma, 2) / 2) * (T / t) + sigma * Math.Sqrt(T / t) * ran[i, j]);
                      simularOP[i+3*n, j] = simularOP[i+3*n, j - 1] * Math.Exp((r - Math.Pow(1.0001*sigma, 2) / 2) * (T / t) + 1.0001*sigma * Math.Sqrt(T / t) * ran[i, j]);*/
                }

            }

            return simularOP;
        }
        public static double [,]SimulateAnti(int t, int n, double s, double k, double r, double T, double sigma, double[,] ran)
        {//SIMULATE THE STOCK PRICE FOR ALL KIND OF CALL OR PUT AND USE ANTI
            double[,] simularOP = new double[2*n, t];
            /*double[,] simularT = new double[n, t];
            double[,] simularR = new double[n, t];
            double[,] simularV = new double[n, t];*/

            for (int i = 0; i <2* n; i++)
            {
                simularOP[i, 0] = s;
            }
            for (int i = 0; i < n; i++)
            {
                for (int j = 1; j < t; j++)
                {
                    simularOP[i, j] = simularOP[i, j - 1] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (T / (t - 1)) + sigma * Math.Sqrt(T / (t - 1)) * ran[i, j]);
                    simularOP[i+n, j] = simularOP[i+n, j - 1] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (T / (t - 1)) + sigma * Math.Sqrt(T / (t - 1)) * ran[i, j]*(-1));
                    // simulate the path of stock
                    /* simularOP[i+n, j] = simularOP[i+n, j - 1] * Math.Exp((r - Math.Pow(sigma, 2) / 2) * (1.001*T / t) + sigma * Math.Sqrt(1.001*T / t) * ran[i, j]);
                       simularOP[i+2*n, j] = simularOP[i+2*n, j - 1] * Math.Exp((1.0001*r - Math.Pow(sigma, 2) / 2) * (T / t) + sigma * Math.Sqrt(T / t) * ran[i, j]);
                       simularOP[i+3*n, j] = simularOP[i+3*n, j - 1] * Math.Exp((r - Math.Pow(1.0001*sigma, 2) / 2) * (T / t) + 1.0001*sigma * Math.Sqrt(T / t) * ran[i, j]);*/
                }

            }
            return simularOP;
        }

    }
}
